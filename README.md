# Organizer

`Organizer` is a CLI Python tool to automatically convert a folder plenty of unordered files into topics and subtopics.

## Installation

A package will be done soon. But for the moment you can download the file `organizer.py` and run `python3 organizer.py` to use it.

## Usage

Run `python3 organizer.py`

```
What is the directory> inbox
Please pull all your inputs with the same synthax:                                                                                                     

 • Starts with a Caps                                                                                                                                  

 • Plurial when possible                                                                                                                               
Example: Ideas                                                                                                                                         
───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
What is the topic of README > Open-source
What is the subtopic of README > Documentation
File README moved to inbox/Open-source/Documentation
All files has been made
```

## Contribute

To get all the informations about contributions, please see the [CONTRIBUTING file](./CONTRIBUTING.md)

## License

This project is under GNU-GPL-v3. You can find more informations in the [LICENSE file](./LICENSE).
