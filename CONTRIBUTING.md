# Contributing

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:
The following is a set of guidelines for contributing. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

## Code of Conduct

This project and everyone participating in it is governed by the [Contributor Covenant](./CODE_OF_CONDUCT.md).

## I don't want to read the whole thing I just have a question!!!

Please check the FAQ and the other issues before.

## What should I know before getting started

The software runs over Python 3 and try to be cross-platform. The packages that are used are `os` and `shutil`.

The project is running in **1** script. This runs only in terminal, I don't really plan to make it run in GUI, but If you really want to you can create
an issue to propose it.

To generate the outputs I use the package `rich` but I am open to other ideas of generating a cool output.
Just tell me in the issues what are your thoughts on this.

## How to contribute?

### Reporting bugs

1. Make sure your question / bug cannot be solved using the FAQ and other informations above
2. Check the other issues to know if your issue exist. If it already exist, please, just comment under it and don't create a new one.
3. If your issue is new, then create an issue with a clear and descriptive title.
4. Follow the template for the content:

```markdown
## Environement

* OS:
* Python version: 
* Code version: 

## Actual behaviour

This is what the app gave me

### Logs

This is the error I got and this is the log (crash report):

## Expected behaviour

This is what I would like to do: 

## Reproduction of the issue

To reproduce my issue, follow these steps:

1. 
2. 
3. 

I searched on the web and in the provided resources but I couldn't find my issue. 

Can you help me?

Thanks in advance!
```

5. Please stay active on the issue and close it only when you think the discussion is over.

### Suggestions / Enhancements

1. Check if there is already an issue about it, if so, try to contribute to it by adding your ideas.
2. If the issue doesn't exist, create one with a very clear and descriptive title
3. Follow this template to make your suggestion:

```markdown
## Environement

* OS: 

## The idea

This is the idea I got...

## Step by step description

## Why it would be good to implement it

## Screenshot

This is a screenshot to show you how it could be implemented in the interface:

![screenshot](URLTOSCREENSHOT)
```

4. Please stay active on the issue and close it when you think the discussion is over.


### Your first code contribution

Unsure where to begin contributing to the project? 
1. Start by looking at `help-wanted` issues. These are issues that often doesn't require much code.
2. Click on Fork and clone the repository to make your changes
3. Make sure your contribution is useful for the project
	* Maintain the code quality
	* Fix problems and bugs
	* Create sustainable solutions for the project
4. When you code please respect the [style](#styleguides)
5. Go back on the original project and create a pull request. Be sure to describe your changes and link the issue to the PR. Also take care about the
recommendations of others in the community.

## Styleguides

### Git commit messages

Please use the following synthax for your git commit messages:

* Be descriptive in your commits

### Language

The style of the code itself is **black**

You can make your code with the right synthax by running the following command `black organizer.py`

