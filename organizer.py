import os, shutil
from rich import print
from rich.markdown import Markdown

directory = input("What is the directory> ")
print(Markdown("Please pull all your inputs with the same synthax:"))
print(Markdown("- Starts with a Caps"))
print(Markdown("- Plurial when possible"))
print(Markdown("Example: `Ideas`"))
print(Markdown("---"))

files = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]
for file in files:
    topic = input("What is the topic of %s > " % file)
    sub = input("What is the subtopic of %s > " % file)

    # Changing the path of the file
    path = os.path.join(directory, file)

    # Skip the file if not completed
    if topic == "" or sub == "":
        print("One of the two requirements is not passed, skip.")

    # Otherwise do the following
    else:
        # Try to move the file
        try:
            shutil.move(path, "%s/%s/%s/%s" % (directory, topic, sub, file))
            print("File %s moved to %s/%s/%s" % (file, directory, topic, sub))

        # Or re-try to move the files after creaeting the directory
        except:
            try:
                os.mkdir("%s/%s" % (directory, topic))
            except:
                print("Directory %s/%s cannot be created." % (directory, topic))
            try:
                os.mkdir("%s/%s/%s" % (directory, topic, sub))
            except:
                print("Directory %s/%s/%s cannot be created." % (directory, topic, sub))
            try:
                shutil.move(path, "%s/%s/%s/%s" % (directory, topic, sub, file))
                print(
                    "Folder %s/%s/%s has been created and the file %s has been moved."
                    % (directory, topic, sub, file)
                )
            except:
                print("Cannot move %s to %s/%s/%s" % (file, directory, topic, sub))

print("All files has been made")
